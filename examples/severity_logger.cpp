// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

void init()
{

  logging::add_file_log
    (
     keywords::file_name = "sample_%N.log",
     keywords::rotation_size = 10 * 24,
     keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
     keywords::format = "[%TimeStamp%]: %Message%"
     );


  logging::core::get()->set_filter
    (
     logging::trivial::severity >= logging::trivial::info
     );
}

int main(int, char*[])
{

  init();
  logging::add_common_attributes();

  using namespace logging::trivial;

  src::severity_logger< severity_level > lg;

   BOOST_LOG_SEV(lg, trace) << "A trace severity message";
   BOOST_LOG_SEV(lg, debug) << "A debug severity message";
   BOOST_LOG_SEV(lg, info) << "An informational severity message";
   BOOST_LOG_SEV(lg, warning) << "A warning severity message";
   BOOST_LOG_SEV(lg, error) << "An error severity message";
   BOOST_LOG_SEV(lg, fatal) << "A fatal severity message";


   BOOST_LOG_TRIVIAL(trace) << "A trace severity message";
   BOOST_LOG_TRIVIAL(debug) << "A debug severity message";
   BOOST_LOG_TRIVIAL(info) << "An informational severity message";
   BOOST_LOG_TRIVIAL(warning) << "A warning severity message";
   BOOST_LOG_TRIVIAL(error) << "An error severity message";
   BOOST_LOG_TRIVIAL(fatal) << "A fatal severity message";

  return 0;
}
